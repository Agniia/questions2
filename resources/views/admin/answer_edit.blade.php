@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Изменить вопрос</h4>
    <form action="{{ route('answers.update', ['id' => $question->answer->id ])}}" method="post">
        @method('PUT')
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="question">Вопрос</label>
            <input type="hidden" name="question_id" value="{{ $question->id }}">
            <input type="text" class="form-control" id="question" aria-describedby="question" value="{{ $question->question  }}" name="question" disabled>
       </div>
       <div class="form-group">
            <label for="answer">Ответ на вопрос</label>
            <input type="hidden" name="answer_id" value="{{ $question->answer->id }}">
            <input type="text" class="form-control" id="answer" aria-describedby="answer" value="{{ $question->answer->answer }}" name="answer">
       </div> 
       <div class="form-group">
            <label for="status">Статус вопроса</label>
            <select class="form-control" id="status_id" name="status_id">
              <option value="{{ $question->status->id  }}" selected>{{ $question->status->status }}</option>
              <option value="2" >Опубликован</option>
              <option value="3" >Скрыт</option>
            </select>
       </div> 
       <button type="submit" class="btn btn-primary">Изменить</button>    
   </form>
 </div>   
@endsection
