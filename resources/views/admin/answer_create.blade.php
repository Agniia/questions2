@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Изменить вопрос</h4>
    <form action="{{ route('answers.store' )}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <input type="hidden" name="id_question" value="{{ $question->id }}">
        <div class="form-group">
            <label for="question">Вопрос</label>
            <input type="text" class="form-control" id="question" aria-describedby="question" value="{{ $question->question  }}" name="question" disabled>
       </div>
       <div class="form-group">
            <label for="answer">Ответ на вопрос</label>
            <input type="text" class="form-control" id="answer" aria-describedby="answer" placeholder="Ответ на вопрос" name="answer" required>
       </div> 
       <div class="form-group">
            <label for="status">Статус вопроса</label>
            <select class="form-control" id="status_id" name="status_id">
              <option value="2" >Опубликован</option>
              <option value="3" >Скрыт</option>
            </select>
       </div> 
       <button type="submit" class="btn btn-primary">Сохранить</button>    
   </form>
   
 </div>   
@endsection
