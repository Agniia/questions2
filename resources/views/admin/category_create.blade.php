@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Добавить категорию</h4>
    <form action="{{ route('categories.store')}}" method="post">
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
       <div class="form-group">
            <label for="category">Название категории</label>
            <input type="text" class="form-control" id="category" aria-describedby="answer" placeholder="Название категории" name="category" required>
       </div> 
       <button type="submit" class="btn btn-primary">Сохранить</button>    
   </form>
   
 </div>   
@endsection
