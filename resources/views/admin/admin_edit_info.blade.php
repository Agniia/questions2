@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>{{ $title }}</h4>
<table class="table">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Имя</th>
          <th scope="col">Email</th>
          <th scope="col">Дата изменения пароля</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">{{ $admin['id'] }}</th>
          <td>{{ $admin['name'] }}</td>
          <td>{{ $admin['email'] }}</td>
          <td>{{ $admin['updated_at'] }}</td>
        </tr>
      </tbody>
</table>    
 </div>   
@endsection
