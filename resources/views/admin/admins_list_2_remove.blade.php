@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Список администраторов для удаления</h4>
    <table class="table">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Логин</th>
              <th scope="col">Email</th>
              <th scope="col">Создан</th>
              <th scope="col">Изменен</th>
              <th scope="col">Удалить</th>
            </tr>
         </thead>
        <tbody>
        @foreach($admins as $admin)
        <tr>
          <th scope="row">{{ $admin->id }}</th>
          <td>{{ $admin->name }}</td>
          <td>{{ $admin->email }}</td>
          <td>{{ $admin->created_at }}</td>
          <td>{{ $admin->updated_at }}</td>
          <td>
            <form action="{{ route('admin.destroy', $admin->id ) }}" method="POST">
                @method('DELETE')
                @csrf
                <button class="btn btn-primary">Удалить</button>
            </form>
          </td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
