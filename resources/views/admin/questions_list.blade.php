@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Список вопросов</h4>
    <table class="table">
        <thead>
            <tr>
              <th scope="col">#</th>
              <th scope="col">Вопрос</th>
               <th scope="col">Категория</th>
              <th scope="col">Статус</th>
              <th scope="col">Дата вопроса</th>
              <th scope="col">Перейти к вопросу</th>
            </tr>
         </thead>
        <tbody>
        @foreach($questions as $question)
        <tr>
          <th scope="row">{{  $question->id }}</th>
          <td>{{  $question->question  }}</td>
          <td>{{ $question->category->category }}</td>
          <td>{{ $question->status->status }}</td>
          <td>{{ $question->created_at }}</td>
          <td><a href="{{ route('questions.edit', ['id_question' => $question->id] ) }}" class="btn btn-primary">Перейти</a></td>
        </tr>
        @endforeach
        </tbody>
    </table>
</div>
@endsection
