@extends('home')

@section('admin-content')
<div class="admin-content">
<h4>Изменить пароль администратора</h4>
<table class="table">
    <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">Имя</th>
          <th scope="col">Email</th>
          <th scope="col">Дата изменения пароля</th>
        </tr>
      </thead>
      <tbody>
        <tr>
          <th scope="row">{{ $admin->id }}</th>
          <td>{{ $admin->name }}</td>
          <td>{{ $admin->email }}</td>
          <td>{{ $admin->updated_at }}</td>
        </tr>
      </tbody>
</table> 
    <form action="{{ route('admin.update', ['id_admin' => $admin->id ])}}" method="post">
       @method('PUT')
        <input type="hidden" name="_token" value="{{ csrf_token() }}">
        <div class="form-group">
            <label for="password">Введите новый пароль администратора</label>
            <input type="password" class="form-control" id="password" aria-describedby="password" placeholder="Пароль администратора" name="password" required>
       </div>
       <button type="submit" class="btn btn-primary">Изменить пароль</button>    
   </form>
   
 </div>   
@endsection
