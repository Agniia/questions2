@extends('faq_main')

@section('questions')
<section class="questions">
    <div class="row  m-b-md align-items-center justify-content-between">
            <div class="col-6"><a href="{{ route('categories.show', ['id' => $currentCategory->id] ) }}">Вопросы и ответы категории "{{ $currentCategory->category }}"</a></div>
            <div class="col-6"><a href="{{ route('faq.create', ['category_id' => $currentCategory->id ] ) }}">Задать вопрос по категории  "{{ $currentCategory->category }}"</a></div>
    </div>
    <h3>{{ $author }},</h3>
   <p>ваш вопрос: <i>"{{ $question }}"</i> направлен на обработку. Ответ на него будет отправлен на Вашу электронную почту {{ $email }} </p>
</section>        
@endsection
