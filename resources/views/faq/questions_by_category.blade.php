@extends('faq_main')

@section('questions')
<section class="questions">
    <div class="row  m-b-md align-items-center justify-content-between">    
        @if($questions->isEmpty()) 
            <div class="col-6"><h3>В категории "{{ $category->category }}" 
            нет вопросов для показа</h3></div>
            <div class="col-6"><a href="{{ route('faq.create', ['category_id' =>$category->id ]) }}">Задать вопрос по категории  "{{ $category->category}}"</a></div>
        @else
            <div class="col-6"><h3>Вопросы и ответы категории "{{ $category->category }}"</h3></div>
            <div class="col-6"><a href="{{ route('faq.create', ['category_id' =>$category->id ]) }}">Задать вопрос по категории  "{{ $category->category }}"</a></div>
    @endif
    </div>
    @foreach($questions as $question)
        <div  class="questions__item  m-b-md">
            <p>Вопрос задал: <i>{{ $question->author->name }}</i></p>
        	<p>Дата вопроса: <i>{{ $question->created_at }}</i></p>
        	<h4>Вопрос:</h4>
        	<p>{{ $question->question}}</p>           
        	<h4>Ответ на вопрос:</h4>
        	<p>{{ $question->answer['answer']}}</p>     
        </div>
    @endforeach
</section>        
@endsection
