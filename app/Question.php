<?php

namespace FAQ;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $fillable = array('category_id', 'question', 'status_id', 'author_id');
     
     public function author()
    {
        return $this->belongsTo('FAQ\Author');
    }
    
    public function category()
    {
        return $this->belongsTo('FAQ\Category');
    }
        
    public function status()
    {
        return $this->belongsTo('FAQ\Status');
    }
            
    public function answer()
    {
        return $this->belongsTo('FAQ\Answer');
    }
    
    public function scopeActive($query) {
        return $query->where('status_id', 2);
    }
        
    public function scopeWaiting($query) {
        return $query->where('status_id', 1);
    }
}
