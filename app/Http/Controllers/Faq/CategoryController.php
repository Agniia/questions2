<?php

namespace FAQ\Http\Controllers\Faq;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Category;

class CategoryController extends Controller
{
    public function show($id)
    {
          $category = Category::find($id);
          $questions = $category->questionsFiltered;
          $categories = Category::all();     
          return view('faq.questions_by_category', compact('questions','categories','category'));      
    }
}
