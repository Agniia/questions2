<?php

namespace FAQ\Http\Controllers\Admin;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Question;
use FAQ\Category;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allCategories = Category::all();
       return view('admin.categories_list')->with('allCategories',$allCategories);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.category_create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
           $category = Category::create(array('category' =>$request->category));
           return redirect()->route('categories.index');
    }
    
    public function destroylist()
    {
        $allCategories = Category::all();
        return view('admin.categories_list_2_remove')->with('allCategories',$allCategories);
    }  

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $questionsToDelete = Question::where('category_id', $id)->delete();
      $affectedRows = Category::where('id', $id)->delete();
      return redirect()->route('categories.destroylist');
    }
}
