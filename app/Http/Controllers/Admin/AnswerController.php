<?php

namespace FAQ\Http\Controllers\Admin;

use Illuminate\Http\Request;
use FAQ\Http\Controllers\Controller;
use FAQ\Question;
use FAQ\Answer;

class AnswerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($question_id)
    {
        $question = Question::where('id', $question_id)->first();
        return view('admin.answer_create')->with('question',$question);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $answer = Answer::create(array('answer' =>$request->answer));
        $question = Question::find($request->id_question);
        $question->answer_id = $answer->id;
        $question->status_id = $request->status_id;
        $question->save();
        return redirect()->route('questions.edit', ['id_question' => $request->id_question]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $question = Question::where('id',$id)
         ->with('answer')->with('status')->first();
        return view('admin.answer_edit')->with('question',$question);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $answer = Answer::find($id);
        $answer->answer = $request->answer;
        $answer->save();
        $question = Question::find($request->question_id);
        $question->status_id =  $request->status_id;
        $question->save();
        return redirect()->route('questions.edit', ['id_question' => $request->question_id]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
